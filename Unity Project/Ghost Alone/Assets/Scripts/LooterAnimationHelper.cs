﻿using UnityEngine;
using System.Collections;

public class LooterAnimationHelper : MonoBehaviour 
{
    LooterController looterController;

	// Use this for initialization
	void Start() 
	{
        looterController = transform.parent.GetComponent<LooterController>();
	}
	
	// Update is called once per frame
	void Update() 
	{
		
	}
	
	// FixedUpdate is called less.
	void FixedUpdate()
	{
		
	}

    public void OnTeleport()
    {
        looterController.OnTeleport();
    }

    public void FinishTeleporting()
    {
        looterController.FinishTeleporting();
    }

    public void DisableWalking()
    {
        looterController.DisableWalking();
    }

    public void EnableWalking()
    {
        looterController.EnableWalking();
    }

    public void OnDeathEnd()
    {
        looterController.OnFadeOut();
    }
}
