﻿using UnityEngine;
using System.Collections;

public class DoorController : MonoBehaviour 
{
    public int maxDoorHP;
    public string doorTitle = "Portal To Everywhere";
    public float doorForceAffinity = 42;
    public int xpForKillingDoor = 25;
    public int[] xpForDoorLevels;

    bool open = false;
    int looters = 0;
    Animator anim;

	// Use this for initialization
	void Start() 
	{
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update() 
	{
		
	}
	
	// FixedUpdate is called less.
	void FixedUpdate()
	{
		
	}

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag != "Looter")
        {
            return;
        }

        looters++;

        if (!open)
        {
            open = true;
            anim.SetBool("open", true);
        }
    }

    void OnTriggerExit2D(Collider2D c)
    {
        if (c.tag != "Looter")
        {
            return;
        }

        looters--;

        if (open && looters <= 0)
        {
            looters = 0;
            anim.SetBool("open", false);
        }
    }
}
