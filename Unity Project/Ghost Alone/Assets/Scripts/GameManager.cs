﻿using UnityEngine;
using System;
using System.Collections;

public class GameManager : MonoBehaviour 
{
    // stats
    public static int looters = 0;
    public static int trophies = 0;
    public static int objects = 0;
    public static int trophiesStolen = 0;
    public static int objectsBroken = 0;
    public static int lootersKilled = 0;
    public static int lootersScaredAway = 0;

    static LoseReason loseReason;

    static Levels currentLevel;
    static Levels nextLevel;

    public static int highestLevel;

	// Use this for initialization
	void Awake()
	{
        highestLevel = PlayerPrefs.GetInt("Level", 1);

        Debug.Log("Highest level:" + highestLevel);
	}
	
    public static void StartLevel(Levels level)
    {
        currentLevel = level;

        nextLevel = getNextLevel(level);

        ResetStats();
    }

    public static void WinLevel(Levels level)
    {
        Debug.Log("Won level " + level);

        if ((int)level >= highestLevel)
        {
            highestLevel = (int)level;
            highestLevel++;
            Debug.Log("Highest level: " + highestLevel);
            PlayerPrefs.SetInt("Level", highestLevel);
            PlayerPrefs.Save();
        }
    }

    public static bool HasUnlockedLevel(Levels level)
    {
        if (level == Levels.Credits ||
            level == Levels.Intro)
            return true;

        bool ret = (int)level <= highestLevel;
        Debug.Log("Has unlocked level " + level + ": " + ret);
        return ret;
    }

    public static void ResetStats()
    {
        Debug.Log("ResetStats");

        // stats
        looters = 0;
        trophies = 0;
        objects = 0;
        trophiesStolen = 0;
        objectsBroken = 0;
        lootersKilled = 0;
        lootersScaredAway = 0;
    }

	// Update is called once per frame
	void Update() 
	{
		
	}
	
	// FixedUpdate is called less.
	void FixedUpdate()
	{
		
	}

    private static Levels getNextLevel(Levels level)
    {
        Levels ret = Levels.None;

        int index = (int)level;

        index++;

        if (Enum.IsDefined(typeof(Levels), index))
        {
            ret = (Levels)Enum.ToObject(typeof(Levels), index);
        }

        return ret;
    }

    public static string getLevelString(Levels level)
    {
        string ret = "";

        switch (level)
        {
            case Levels.None:
                ret = "";
                break;
            case Levels.Intro:
                ret = "NewIntro";
                break;
            case Levels.Level1:
                ret = "Level 1";
                break;
            case Levels.Level2:
                ret = "Level 1.5";
                break;
            case Levels.Level3:
                ret = "TinaLevelA";
                break;
            case Levels.Level4:
                ret = "Level 2";
                break;
            case Levels.Level5:
                ret = "Level 3";
                break;
            case Levels.Level6:
                ret = "TannersLevel";
                break;
            case Levels.Level7:
                ret = "DerricksLevel";
                break;
            case Levels.Credits:
                ret = "Credits";
                break;
        }

        return ret;
    }

    public static Levels getCurrentLevel()
    {
        Levels ret = Levels.None;

        switch (Application.loadedLevelName)
        {
            case ("NewIntro"):
                ret = Levels.Intro;
                break;
            case ("Level 1"):
                ret = Levels.Level1;
                break;
            case ("Level 1.5"):
                ret = Levels.Level2;
                break;
            case ("TinaLevelA"):
                ret = Levels.Level3;
                break;
            case ("Level 2"):
                ret = Levels.Level4;
                break;
            case ("Level 3"):
                ret = Levels.Level5;
                break;
            case ("TannersLevel"):
                ret = Levels.Level6;
                break;
            case ("DerricksLevel"):
                ret = Levels.Level7;
                break;
            case ("Credits"):
                ret = Levels.Credits;
                break;
        }

        return ret;
    }

    public static string getCurrentLevelString()
    {
        return (getLevelString(currentLevel));
    }

    public static string getNextLevelString()
    {
        return (getLevelString(nextLevel));
    }

    public static void LooterLeft(LoseReason reason)
    {
        if (reason == LoseReason.TutorialOver)
        {
            GameManager.ResetStats();
            Application.LoadLevel("Level 1");
            return;
        }

        Debug.Log("A looter left.");
        switch (reason)
        {
            case LoseReason.Killed:
                Debug.Log("He was killed.");
                lootersKilled++;
                break;
            case LoseReason.StoleLoot:
                Debug.Log("He stole loot.");
                trophiesStolen++;
                break;
            case LoseReason.Retreated:
                Debug.Log("He retreated.");
                lootersScaredAway++;
                break;
        }

        Debug.Log("lootersKilled:" + lootersKilled);
        Debug.Log("lootersScaredAway:" + lootersScaredAway);
        Debug.Log("trophiesStolen:" + trophiesStolen);
        Debug.Log("looters:" + looters);

        if (lootersKilled + lootersScaredAway + trophiesStolen >= looters)
        {
            if (trophiesStolen == 0 &&
                currentLevel != Levels.Credits)
            {
                WinLevel(currentLevel);
            }

            Application.LoadLevel("ResultsScreen");
        }
    }

    public static void Reset()
    {
        currentLevel = Levels.None;
        nextLevel = Levels.None;
    }

    public enum Levels
    {
        None = 0,
        Intro = 1,
        Level1 = 2,
        Level2 = 3,
        Level3 = 4,
        Level4 = 5,
        Level5 = 6,
        Level6 = 7,
        Level7 = 8,
        Credits = 9,
    }

    public enum LoseReason
    {
        None,
        StoleLoot,
        Killed,
        Retreated,
        TutorialOver
    }
}
