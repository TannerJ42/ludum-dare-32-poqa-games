﻿using UnityEngine;
using System.Collections;

public class LooterController : MonoBehaviour
{
    float currentSpeed = 0f;
    public float walkSpeed = 0.5f;
    public float runSpeed = 4f;
    public PathController path;
    public WaypointController currentWaypoint;
    public State state = State.Idle;
    public Animator anim;

    public GameObject healthBar;

    public float runThreshold = 0.25f;

    bool facingRight = true;

    public int startingHP = 100;
    int currentHP = 100;

    public GameObject trophy;
    GameObject trophyObject;
    bool canWalk = true;

    bool retreating = false;

    public AudioSource leaveClip;

    float startMoving;

    GameManager.LoseReason loseReason = GameManager.LoseReason.None;
    private bool hasTrophy = false;

	// Use this for initialization
	void Start() 
	{
        GameManager.looters++;

        anim = GetComponentInChildren<Animator>();

        if (!anim)
        {
            throw new System.Exception(name + " has no animator!");
        }

        currentHP = startingHP;

        startMoving = Time.timeSinceLevelLoad + 3f;
	}
	
	// Update is called once per frame
	void Update() 
	{
		switch (state)
        {
            case State.Idle:
                if (Time.timeSinceLevelLoad > startMoving &&
                    !currentWaypoint &&
                    path)
                {
                    if (path &&
                        path.entryWaypoint)
                    {
                        currentWaypoint = path.entryWaypoint;
                        state = State.Advancing;
                    }
                    path = null;
                }
                break;
            case State.Advancing:
                {
                    if (!currentWaypoint)
                    {
                        state = State.Idle;
                    }
                }
                break;
            case State.Retreating:
                {
                    if (!currentWaypoint)
                    {
                        //Destroy(this.gameObject);
                    }
                }
                break;
        }
	}
	
	// FixedUpdate is called less.
	void FixedUpdate()
	{
        currentSpeed = 0;

        switch (state)
        {
            case State.Idle:
                break;
            case State.Teleporting:
                MoveTowardWaypoint(runSpeed);
                break;
            case State.Advancing:
                MoveTowardWaypoint(walkSpeed);
                break;
            case State.Retreating:
                MoveTowardWaypoint(runSpeed);
                break;
        }

        anim.SetFloat("speed", currentSpeed);
	}

    public enum State
    {
        Idle,
        Advancing,
        Retreating,
        Teleporting,
        Leaving,
    }

    private void MoveTowardWaypoint(float speed)
    {
        currentSpeed = speed;

        if (!currentWaypoint ||
            !canWalk ||
            state == State.Teleporting)
        {
            return;
        }

        Vector3 position = transform.position;
        Vector3 destination = currentWaypoint.transform.position;
        destination.y = position.y;
        destination.z = position.z;

        Vector3 direction = destination - position;
        direction.y = 0;
        direction.z = 0;
        direction.Normalize();

        if (facingRight && direction.x == -1)
        {
            Flip();
        }

        if (!facingRight && direction.x == 1)
        {
            Flip();
        }

        Vector3 newPosition = position + direction * speed * Time.fixedDeltaTime;

        if (Vector2.Distance(position, destination) <=
            speed * Time.fixedDeltaTime)
        {
            if (state == State.Advancing)
            {
                OnReachWaypoint(currentWaypoint.Forward);
            }
            else
            {
                OnReachWaypoint(currentWaypoint.Back);
            }
            newPosition.x = destination.x;
        }

        transform.position = newPosition;
    }

    public void OnFadeOut()
    {
        GameManager.LooterLeft(loseReason);

        Destroy(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (state == State.Retreating)
            return;

        if (!hasTrophy &&
            c.tag == "Trophy")
        {
            OnGetTrophy(c.gameObject);
        }
    }

    private void OnGetTrophy(GameObject go)
    {

        retreating = true;

        Vector3 scale = go.transform.localScale;

        hasTrophy = true;

        SpriteRenderer trophyRenderer = trophy.gameObject.GetComponent<SpriteRenderer>();
        SpriteRenderer gameObjectRenderer = go.gameObject.GetComponent<SpriteRenderer>();

        trophyRenderer.sprite = gameObjectRenderer.sprite;

        trophyObject = go;
        go.SetActive(false);

        scale *= 10;

        trophy.transform.localScale = scale;

        OnBeginRetreating();
    }

    void Flip()
    {
        Vector3 scale = transform.localScale;

        scale.x = -scale.x;

        transform.localScale = scale;

        facingRight = !facingRight;
    }

    void OnReachWaypoint(WaypointController next)
    {
        if (!currentWaypoint)
        {
            return;
        }

        if (!next)
        {

            if (state == State.Retreating)
            {
                if (leaveClip)
                {
                    leaveClip.Play();
                }

                if (hasTrophy)
                {
                    loseReason = GameManager.LoseReason.StoleLoot;
                }
                else
                {
                    loseReason = GameManager.LoseReason.Retreated;
                }
                gameObject.layer = LayerMask.NameToLayer("Trash");
                state = State.Leaving;
                healthBar.SetActive(false);
                anim.SetTrigger("fade");
            }
            else if (state == State.Advancing)
            {
                OnBeginRetreating();
            }

            return;
        }

        if ((state == State.Advancing &&
            currentWaypoint.teleportForward) ||
            (state == State.Retreating &&
            currentWaypoint.teleportBack))
        {
            state = State.Teleporting;
            gameObject.layer = LayerMask.NameToLayer("Trash");
            anim.SetTrigger("teleport");
            ClearTriggers();
            Debug.Log("Setting anim waypoint.");
            return;
        }

        if (currentWaypoint.trigger != "")
        {
            anim.SetTrigger(next.trigger);
        }
        
        currentWaypoint = next;
    }

    void OnBeginRetreating()
    {
        retreating = true;

        if (state == State.Retreating)
        {
            return;
        }

        state = State.Retreating;

        anim.ResetTrigger("look");

        if (currentWaypoint)
        {
            currentWaypoint = currentWaypoint.Back;
        }
        else
        {
            Debug.Log("No currentWaypoint in OnBeginRetreating. Bug!");
        }
    }

    public void OnTeleport()
    {
        Debug.Log("OnTeleport");
        gameObject.layer = LayerMask.NameToLayer("Looter");

        if (!retreating)
        {
            Teleport(currentWaypoint.Forward);
        }
        else
        {
            Teleport(currentWaypoint.Back);
        }
        Flip();
    }

    public void FinishTeleporting()
    {
        Debug.Log("FinishTeleporting");
        if (retreating)
        {
            state = State.Retreating;
        }
        else
        {
            state = State.Advancing;
        }

        if (state == State.Retreating)
        {
            currentWaypoint = currentWaypoint.Back;
        }
        else
        {
            currentWaypoint = currentWaypoint.Forward;
        }
    }

    void Teleport(WaypointController destination)
    {
        if (!destination)
        {
            return;
        }

        var pos = transform.position;
        var dest = destination.transform.position;

        dest.z = pos.z;
        
        transform.position = dest;
    }

    public void DisableWalking()
    {
        canWalk = false;
    }

    public void EnableWalking()
    {
        canWalk = true;
    }

    public float getHealthPercentage()
    {
        return (float)currentHP / startingHP;
    }

    public void TakeDamage(int damage)
    {
        if (state == State.Teleporting ||
            state == State.Leaving)
        {
            return;
        }

        currentHP -= damage;
        if (currentHP <= 0)
        {
            ClearTriggers();

            if (Application.loadedLevelName == "NewIntro")
            {
                loseReason = GameManager.LoseReason.TutorialOver;
            }
            else
            {
                loseReason = GameManager.LoseReason.Killed;
            }

            anim.SetTrigger("death");
            gameObject.layer = LayerMask.NameToLayer("Trash");
            return;
        }
        if ((float)currentHP / startingHP <= runThreshold)
        {
            OnGetScared();
        }

        anim.SetTrigger("takeDamage");
    }

    private void OnGetScared()
    {        
        anim.SetBool("scared", true);
        OnBeginRetreating();
        if (hasTrophy)
        {
            trophy.SetActive(false);
            hasTrophy = false;
            if (trophyObject != null)
            {
                trophyObject.transform.parent = null;
                trophyObject.transform.position = new Vector3(trophy.transform.position.x, trophy.transform.position.y, trophyObject.transform.position.z);
                trophyObject.SetActive(true);
            }
        }
    }

    public void ClearTriggers()
    {
        anim.ResetTrigger("takeDamage");
    }
}
