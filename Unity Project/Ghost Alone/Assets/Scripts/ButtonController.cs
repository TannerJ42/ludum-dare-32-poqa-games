﻿using UnityEngine;
using System.Collections;

public class ButtonController : MonoBehaviour 
{
    public OnClickAction onClickAction;

    public string lineBreak = "line\nbreak";

	// Use this for initialization
	void Start() 
	{
		
	}
	
	// Update is called once per frame
	void Update() 
	{

	}
	
	// FixedUpdate is called less.
	void FixedUpdate()
	{
		
	}

    public void OnClick()
    {
        if (Application.loadedLevelName == "NewIntro")
        {
            GameManager.ResetStats();
        }

        switch(onClickAction)
        {
            case OnClickAction.MainMenu:
                Application.LoadLevel("MainMenu");
                break;
            case OnClickAction.Intro:
                GameManager.Reset();
                Application.LoadLevel("NewIntro");
                break;
            case OnClickAction.StartGame:
                GameManager.Reset();
                Application.LoadLevel("Level 1");
                break;
            case OnClickAction.Results:
                Application.LoadLevel("ResultsScreen");
                break;
            case OnClickAction.Credits:
                Application.LoadLevel("Credits");
                break;
            case OnClickAction.NextLevel:
                string nextLevel = GameManager.getNextLevelString();
                Application.LoadLevel(nextLevel);
                break;
            case OnClickAction.ReplayLevel:
                string currentLevel = GameManager.getCurrentLevelString();
                Application.LoadLevel(currentLevel);
                break;
            case OnClickAction.LevelSelect:
                Application.LoadLevel("LevelSelect");
                break;
        }
    }

    void ChangeScene(string scene)
    {
        Application.LoadLevel(scene);
    }

    public enum OnClickAction
    {
        MainMenu,
        Intro,
        StartGame,
        Results,
        NextLevel,
        ReplayLevel,
        Credits,
        LevelSelect,
    }
}
