﻿using UnityEngine;
using System.Collections;

public class RandomLook : MonoBehaviour 
{
    Animator anim;

    public float timeBetweenChecks = 5f;

    public float lookChance = 0.5f;

    public LooterController looterController;

	// Use this for initialization
	void Start() 
	{
        anim = GetComponentInChildren<Animator>();

        if (!anim)
        {
            throw new System.Exception(name + " has no animator!");
        }

        InvokeRepeating("CheckForLook", timeBetweenChecks, timeBetweenChecks);
	}
	
	// Update is called once per frame
	void Update() 
	{
		
	}
	
	// FixedUpdate is called less.
	void FixedUpdate()
	{
		
	}

    void CheckForLook()
    {
        if (Random.Range(0f, 1f) >= lookChance)
        {
            Look();
        }
    }

    void Look()
    {
        anim.SetTrigger("look");
    }
}
