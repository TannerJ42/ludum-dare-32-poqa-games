﻿using UnityEngine;
using System.Collections;

public class LevelLauncher : MonoBehaviour 
{
    public string levelToLaunch = "Level 1";
    bool locked = true;

	// Use this for initialization
	void Start () 
    {
	    
	}
	
	// Update is called once per frame
	void Update () 
    {
	    
	}

    public void OnClick()
    {
        if (locked)
            return;

        try
        {
            Application.LoadLevel(levelToLaunch);
        }
        catch (System.Exception e)
        {
            Debug.Log("Unable to load level " + levelToLaunch);
            Debug.Log(e.Message);
        }
    }

    public void Unlock()
    {
        locked = false;
    }
}
