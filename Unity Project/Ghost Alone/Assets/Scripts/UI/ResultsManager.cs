﻿using UnityEngine;
using System.Collections;

public class ResultsManager : MonoBehaviour 
{
    public ResultsLineManager lootProtected;
    public ResultsLineManager lootersKilled;
    public ResultsLineManager objectsDestroyed;
    public ResultsLineManager lootersScaredAway;
    public TextMesh message;
    public GameObject retryButton;
    public GameObject quitButton;
    public GameObject nextButton;

    public GameObject fireworks;

    ResultsLineManager[] fields;

    FireworksAudio fireworksAudio;

    public float initialWaitTime = 4f;
    public float timeBetweenLines = 1.5f;

    int currentIndex = 0;

    bool won = false;

    public float fireworkSoundDelay = 2f;

	// Use this for initialization
	void Start() 
	{
        fireworksAudio = GetComponent<FireworksAudio>();

        message.text = "";

        fireworks.SetActive(false);

        lootProtected.AssignNumbers(GameManager.trophies - GameManager.trophiesStolen, GameManager.trophies);
        lootersKilled.AssignNumbers(GameManager.lootersKilled, GameManager.looters);
        lootersScaredAway.AssignNumbers(GameManager.lootersScaredAway, GameManager.looters);
        objectsDestroyed.AssignNumbers(GameManager.objectsBroken, GameManager.objects);

        fields = new ResultsLineManager[4]
        {
            objectsDestroyed,
            lootersScaredAway,
            lootersKilled,
            lootProtected,
        };

        foreach (ResultsLineManager r in fields)
        {
            r.gameObject.SetActive(false);
        }

        Invoke("ActivateCurrentLine", initialWaitTime);

        if (GameManager.trophiesStolen > 0)
        {
            won = false;
        }
        else
        {
            won = true;
        }


        GameManager.ResetStats();

        retryButton.SetActive(false);
        quitButton.SetActive(false);
        nextButton.SetActive(false);
	}
	
	// Update is called once per frame
	void Update() 
	{
        if (currentIndex > fields.Length - 1)
            return;

        if (fields[currentIndex].finished)
        {
            currentIndex++;


            if (fields.Length == currentIndex)
            {
                Invoke("FinishDisplay", timeBetweenLines);
            }
            else
            {
                Invoke("ActivateCurrentLine", timeBetweenLines);
            }
        }
	}
	
    void FinishDisplay()
    {
        if (won)
        {
            //fireworks.enable
            message.text = "You protected your loot!";
            retryButton.SetActive(true);
            quitButton.SetActive(true);
            nextButton.SetActive(true);
            fireworks.SetActive(true);
            fireworksAudio.PlayFireworkLoop(fireworkSoundDelay);
        }
        else
        {
            message.text = "Looters stole some loot!\n     Try again!";
            retryButton.SetActive(true);
            quitButton.SetActive(true);
            nextButton.SetActive(false);
        }
    }

    void ActivateCurrentLine()
    {
        fields[currentIndex].gameObject.SetActive(true);
        fields[currentIndex].StartCounting();
    }

	// FixedUpdate is called less.
	void FixedUpdate()
	{
		
	}
}
