﻿using UnityEngine;
using System.Collections;

public class CountUpText : MonoBehaviour 
{
    public int startingNumber = 0;
    public int endingNumber = 100;

    int currentNumber = 0;

    float startingTickSpeed = 0.04f;
    float tickSpeedIncrease = 1.0f;
    float tickSpeedIncreaseThreshold = 1.0f;

    float tickSpeed;

    TextMesh text;

    public bool finishedCounting = false;

	// Use this for initialization
	void Start()
	{

	}
	
	// Update is called once per frame
	void Update() 
	{
		
	}
	
	// FixedUpdate is called less.
	void FixedUpdate()
	{
		
	}

    public void StartCounting()
    {
        text = GetComponentInChildren<TextMesh>();
        tickSpeed = startingTickSpeed;

        if (endingNumber == 0)
        {
            finishedCounting = true;
            return;
        }

        Increment();
    }

    void Increment()
    {
        currentNumber++;

        text.text = currentNumber.ToString();

        if (currentNumber < endingNumber)
        {
            if (currentNumber > endingNumber * tickSpeedIncreaseThreshold)
            {
                tickSpeed *= tickSpeedIncrease;
            }
            
            Invoke("Increment", tickSpeed);
        }
        else
        {
            finishedCounting = true;
        }
    }
}
