﻿using UnityEngine;
using System.Collections;

public class ClickController : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            ClickAtPoint(mousePosition);
        }
	}

    void ClickAtPoint(Vector2 position)
    {
        Collider2D[] col = Physics2D.OverlapPointAll(position);

        foreach (Collider2D c in col)
        {
            if (c &&
                c.transform &&
                c.transform.gameObject)
            {
                ButtonController bc = c.transform.gameObject.GetComponentInChildren<ButtonController>();

                if (bc)
                {
                    bc.OnClick();
                }

                LevelLauncher ll = c.transform.gameObject.GetComponentInChildren<LevelLauncher>();

                if (ll)
                {
                    ll.OnClick();
                }
            }
        }
    }
}
