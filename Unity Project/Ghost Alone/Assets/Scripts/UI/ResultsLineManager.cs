﻿using UnityEngine;
using System.Collections;

public class ResultsLineManager : MonoBehaviour 
{
    public bool finished = false;

    CountUpText[] countUpTexts;

	// Use this for initialization
	void Start() 
	{
       
	}
	
	// Update is called once per frame
	void Update() 
	{
        finished = true;

        foreach (CountUpText ct in countUpTexts)
        {
            if (!ct.finishedCounting)
            {
                finished = false;
            }
        }
	}
	
    public void AssignNumbers(int left, int right)
    {
        countUpTexts = GetComponentsInChildren<CountUpText>();

        foreach (CountUpText ct in countUpTexts)
        {
            if (ct.name == "Left Counter")
            {
                ct.endingNumber = left;
            }
            else if (ct.name == "Right Counter")
            {
                ct.endingNumber = right;
            }
            else
            {
                Debug.Log("Couldn't assign number to " + ct.name);
            }
        }
    }

    public void StartCounting()
    {
        foreach (CountUpText ct in countUpTexts)
        {
            ct.StartCounting();
        }
    }

	// FixedUpdate is called less.
	void FixedUpdate()
	{
		
	}
}
