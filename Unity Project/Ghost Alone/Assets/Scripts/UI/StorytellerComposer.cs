﻿using UnityEngine;
using System.Collections;

public class StorytellerComposer : MonoBehaviour 
{
    Animator anim;
    public TextMesh message;

    public GameObject room;
    public GameObject throwables;
    public GameObject looter;
    public GameObject looter2;
    public GameObject skipButton;

    public AudioSource clip;

    string[] messages;

    Part part;

	// Use this for initialization
	void Start() 
	{
        messages = new string[5]
        {
            "Tap and hold\nto move.",
            "Looters are trying\nto steal your stuff!",
            "But ghosts can't\ndirectly touch them.",
            "Push items at\nthe looters.",
            "Drive them away,\nor kill them.",
        };

        anim = GetComponent<Animator>();
        Advance();

        clip = GetComponent<AudioSource>();

	}
	
	// Update is called once per frame
	void Update() 
	{
		
	}
	
	// FixedUpdate is called less.
	void FixedUpdate()
	{
		
	}

    void Advance()
    {
        message.text = messages[(int)part];

        float time = 5f;

        switch(part)
        {
            case Part.Part1:
                if(clip)
                {
                    clip.Play();
                }
                time = 3f;
                room.SetActive(true);
                break;
            case Part.Part2:
                skipButton.SetActive(true);
                looter.SetActive(true);
                break;
            case Part.Part3:
                time = 3f;
                break;
            case Part.Part4:
                looter.SetActive(false);
                looter2.SetActive(true);
                throwables.SetActive(true);
                break;
            case Part.Part5:
                break;
        }

        anim.SetTrigger("FadeIn");
        Invoke("FadeOut", time);
    }

    void FadeOut()
    {
        anim.SetTrigger("FadeOut");
        part += 1;

        if ((int)part <= messages.Length - 1)
        {
            Invoke("Advance", 2f);
        }
    }

    void Begin()
    {
        GameManager.ResetStats();
        Application.LoadLevel("Level 1");
    }

    enum Part
    {
        Part1 = 0,
        Part2 = 1,
        Part3 = 2,
        Part4 = 3,
        Part5 = 4,
    }
}
