﻿using UnityEngine;
using System.Collections;

public class LevelUnlockController : MonoBehaviour 
{
    public GameManager.Levels level;

    public GameObject locked;
    public GameObject unlocked;

	void Start () 
    {
	    if (GameManager.HasUnlockedLevel(level))
        {
            Debug.Log("Locked");
            locked.SetActive(false);
            unlocked.SendMessage("Unlock", SendMessageOptions.DontRequireReceiver);
            unlocked.SetActive(true);
        }
        else
        {
            locked.SetActive(true);
            unlocked.SetActive(false);
        }

        LevelLauncher ll = GetComponentInChildren<LevelLauncher>();
        if (ll != null)
        {

            ll.levelToLaunch = GameManager.getLevelString(level);
        }
	}
}
