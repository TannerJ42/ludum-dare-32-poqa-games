﻿using UnityEngine;
using System.Collections;

public class ThrowableController : MonoBehaviour 
{
    public int damage = 10;
    public int hp = 1;

    State state = State.Idle;

    Animator anim;
    Rigidbody2D rb;

    public float recoveryTime = 1f;
    float recoversAt;

    public float minimumChaosTime;

	// Use this for initialization
	void Start() 
	{
        GameManager.objects++;

        anim = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void LateUpdate() 
	{
		switch (state)
        {
            case State.Recovering:
                if (Time.timeSinceLevelLoad >= recoversAt)
                {
                    state = State.Idle;
                }
                break;
            case State.Weaponized:
                if (Time.timeSinceLevelLoad >= minimumChaosTime &&
                    rb.velocity.magnitude <= 1f)
                {
                    OnStopBeingWeaponized();
                }
                break;
        }
	}
	
	// FixedUpdate is called less.
	void FixedUpdate()
	{
		
	}

    void TakeDamage(int damage)
    {
        if (hp <= 0)
            return;

        hp -= damage;

        if (hp <= 0)
        {
            GameManager.objectsBroken++;
            anim.SetTrigger("destroy");
            gameObject.layer = LayerMask.NameToLayer("Trash");
            Destroy(this.gameObject, 1f);
        }
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if (state != State.Weaponized)
        {
            return;
        }

        if (c.gameObject.tag == "Throwable")
        {
            return;
        }

        if (c.gameObject.tag == "Looter")
        {
            c.gameObject.SendMessage("TakeDamage", damage);
            TakeDamage(1);
        }
    }

    void OnStopBeingWeaponized()
    {
        state = State.Recovering;
        recoversAt = Time.timeSinceLevelLoad + recoveryTime;
        gameObject.layer = LayerMask.NameToLayer("Peace");
    }

    public enum State
    {
        Idle,
        Weaponized,
        Recovering,
    }

    internal void OnGetThrown(Vector2 v, float pushForce)
    {
        if (rb.isKinematic)
            rb.isKinematic = false;

        Vector3 direction = transform.position - (Vector3)v;
        rb.AddForce(direction * pushForce);

        state = State.Weaponized;

        gameObject.layer = LayerMask.NameToLayer("Chaos");

        minimumChaosTime = Time.timeSinceLevelLoad + 0.2f;
    }
}
