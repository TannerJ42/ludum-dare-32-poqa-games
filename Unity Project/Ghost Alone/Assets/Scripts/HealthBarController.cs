﻿using UnityEngine;
using System.Collections;

public class HealthBarController : MonoBehaviour 
{
    public GameObject background;
    public GameObject foreground;

    LooterController looter;

	// Use this for initialization
	void Start() 
	{
        looter = transform.parent.GetComponent<LooterController>();
	}
	
	// Update is called once per frame
	void Update() 
	{
        float pct = looter.getHealthPercentage();

        if (pct <= 0)
        {
            gameObject.SetActive(false);
        }

        Vector3 scale = foreground.transform.localScale;

        scale.x = pct;

        foreground.transform.localScale = scale;
	}
	
	// FixedUpdate is called less.
	void FixedUpdate()
	{
		
	}
}
