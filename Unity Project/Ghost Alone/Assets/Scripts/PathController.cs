﻿using UnityEngine;
using System.Collections;

public class PathController : MonoBehaviour
{
    public WaypointController entryWaypoint;
    public int firstWaypoint = 0;

    // Use this for initialization
    void Start()
    {
        WaypointController[] waypoints = GetComponentsInChildren<WaypointController>();

        for (int i = 0; i <= waypoints.Length - 1; i++)
        {
            WaypointController p = waypoints[i].GetComponent<WaypointController>();
            
            if (i > 0)
            {
                p.Back = waypoints[i - 1];
            }

            if (i <= waypoints.Length - 2)
            {
                p.Forward = waypoints[i + 1];
            }
        }

        entryWaypoint = waypoints[firstWaypoint];
    }
}
