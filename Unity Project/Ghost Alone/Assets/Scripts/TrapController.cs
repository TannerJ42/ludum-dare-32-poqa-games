﻿using UnityEngine;
using System.Collections.Generic;

public class TrapController : MonoBehaviour 
{
    State state = State.Idle;

    private bool clickedThisFrame = false;

    public GameObject scarePrefab;

    public bool repeatable = false;

    public float timeBetweenTriggers = 2f;

    Animator anim;

	// Use this for initialization
	void Start() 
	{
        anim = GetComponentInChildren<Animator>();

        if (!anim)
        {
            throw new System.Exception("Trap with name " + gameObject.name + " doesn't have an animator.");
        }
	}
	
	// Update is called once per frame
	void Update() 
	{
		if (state == State.Primed &&
            !clickedThisFrame &&
            Input.GetMouseButtonDown(0))
        {
            state = State.Idle;
        }
	}

    void LateUpdate()
    {
        clickedThisFrame = false;
    }

	// FixedUpdate is called less.
	void FixedUpdate()
	{
		
	}

    void OnMouseDown()
    {
        if (state != State.Triggered)
        {
            state = State.Primed;
        }
        clickedThisFrame = true;
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (state != State.Primed ||
            c.tag != "Player")
        {
            return;
        }

        Collider2D[] colliders = GetComponentsInChildren<Collider2D>();

        foreach (Collider2D co in colliders)
        {
            co.enabled = false;
        }

        anim.SetTrigger("Trigger");
    }

    public void OnScare()
    {
        GameObject scare = (GameObject)Instantiate(scarePrefab, transform.position, Quaternion.identity);
        Destroy(scare, 0.1f);

        state = State.Triggered;

        if (repeatable)
        {
            Invoke("Reset", timeBetweenTriggers);
        }
    }

    void Reset()
    {
        state = State.Idle;
    }

    public enum State
    {
        Idle,
        Primed,
        Triggered,
    }
}
