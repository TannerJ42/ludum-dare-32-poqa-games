﻿using UnityEngine;
using System.Collections;

public class GhostController : MonoBehaviour 
{
    public Vector3 currentDestination;
    public float minimumSpeed = 0.1f;
    public float rampUpSpeed = 1.1f;
    public float rampDownSpeed = 0.9f;
    public float maxSpeed = 5;
    float currentSpeed = 0;

    Animator anim;

    public State state = State.Idle;

    public float force = 100f;

    bool tapping = false;
    bool facingRight = false;

    public float rotationClamp = 45;

    GhostAudioController audioController;

    public GameObject topLeft;
    public GameObject bottomRight;

    void Awake()
    {
        GameManager.StartLevel(GameManager.getCurrentLevel());
    }

	// Use this for initialization
	void Start() 
 	{

        audioController = GetComponent<GhostAudioController>();

        anim = GetComponentInChildren<Animator>();

        if (!anim)
        {
            throw new System.Exception("Ghost has no animator!");
        }
	}
	
	// Update is called once per frame
	void Update() 
	{
		if (Input.GetMouseButton(0))
        {
            tapping = true;
            state = State.Pathing;

            currentDestination = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            currentDestination.z = 0;
        }
        else
        {
            tapping = false;
        }

        anim.SetFloat("speed", currentSpeed);
        float x = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;



        if (facingRight && x < transform.position.x)
        {
            Flip();
        }
        else if (!facingRight && x > transform.position.x)
        {
            Flip();
        }

        Rotate();
	}

    void Rotate()
    {
        var mousePosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));

        var direction = mousePosition - transform.position;

        int flipAdjustment = facingRight ? 0 : 180;

        //Rotates toward the mouse
        transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg + flipAdjustment);
 
    }
	
	// FixedUpdate is called less.
	void FixedUpdate()
	{
        switch (state)
        {
		    case State.Idle:
                break;
            case State.Pathing:
                AdjustSpeed();
                MoveTowardTarget();
                break;
        }
	}

    void AdjustSpeed()
    {
        if (tapping)
        {
            if (currentSpeed < minimumSpeed)
            {
                currentSpeed = minimumSpeed;
            }
            else
            {
                currentSpeed *= rampUpSpeed;
            }

        }
        else
        {
            if (currentSpeed < minimumSpeed)
            {
                currentSpeed = 0;
                state = State.Idle;
            }
            else
            {
                currentSpeed *= rampDownSpeed;
            }
        }

        if (currentSpeed > maxSpeed)
        {
            currentSpeed = maxSpeed;
        }
    }

    void MoveTowardTarget()
    {
        Vector3 position = transform.position;
        Vector3 destination = currentDestination;
        Vector3 direction = currentDestination - position;
        Vector3 newPosition;
        direction.z = 0;
        direction.Normalize();

        destination.z = position.z;

        newPosition = position + direction * currentSpeed * Time.deltaTime;

        if (newPosition.x < topLeft.transform.position.x)
        {
            newPosition.x = topLeft.transform.position.x;
        }
        if (newPosition.x > bottomRight.transform.position.x)
        {
            newPosition.x = bottomRight.transform.position.x;
        }
        if (newPosition.y > topLeft.transform.position.y)
        {
            newPosition.y = topLeft.transform.position.y;
        }
        if (newPosition.y < bottomRight.transform.position.y)
        {
            newPosition.y = bottomRight.transform.position.y;
        }

        transform.position = newPosition;

        // if we went past our destination
        if (Vector2.Distance(position, destination) <=
            Vector2.Distance(position, newPosition))
        {
            transform.position = destination;
            OnReachedDestination();
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "Throwable")
        {
            ThrowableController tc = c.GetComponent<ThrowableController>();

            if (tc)
            {
                if (audioController)
                {
                    audioController.PlayRandomBumpEffect();
                }
                else
                {
                    Debug.Log("No audio controller on ghost! Victor's gonna be PISSED!");
                }
                tc.OnGetThrown(transform.position, force);
            }
        }
    }

    private void OnReachedDestination()
    {
        state = State.Idle;
        currentSpeed = 0;
    }

    public enum State
    {
        Idle,
        Pathing,
    }

    void Flip()
    {
        Vector3 scale = transform.localScale;

        scale.x = -scale.x;

        transform.localScale = scale;

        facingRight = !facingRight;
    }
}
