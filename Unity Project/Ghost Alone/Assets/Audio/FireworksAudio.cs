﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FireworksAudio : MonoBehaviour
{
    public AudioClip[] effects;
    AudioClip[] clips;
    List<AudioSource> sources;

    // Use this for initialization
    void Start()
    {
        sources = new List<AudioSource>(gameObject.GetComponents<AudioSource>());

        sources.Add(gameObject.AddComponent<AudioSource>());
    }

    void PlaySound(int number)
    {
        if (clips.Length >= number)
        {
            AudioClip clip = clips[number];

            AddClip(clip);
        }
    }

    void AddClip(AudioClip clip)
    {
        for (int i = 0; i <= sources.Count - 1; i++)
        {
            if (!IsPlaying(sources[i]))
            {
                sources[i].clip = clip;
                sources[i].loop = true;
                sources[i].Play();
                return;
            }
        }

        sources.Add(gameObject.AddComponent<AudioSource>());
    }

    public void PlayFireworkLoop(float time)
    {
        Invoke("PlaySound", time);
    }

    private void PlaySound()
    {
        if (effects.Length > 0)
        {
            AudioClip clip = effects[0];

            AddClip(clip);
        }
    }

    bool IsPlaying(AudioSource source)
    {
        if (source.clip == null)
            return false;
        if (!source.isPlaying)
            return false;
        return true;
    }
}