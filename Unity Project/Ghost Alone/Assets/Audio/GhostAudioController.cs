﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GhostAudioController : MonoBehaviour
{
    public AudioClip[] bumpEffects;
    AudioClip[] clips;
    List<AudioSource> sources;

    // Use this for initialization
    void Start()
    {
        sources = new List<AudioSource>(gameObject.GetComponents<AudioSource>());
        sources.Add(gameObject.AddComponent<AudioSource>());
    }

    public void PlaySound(int number)
    {
        if (clips.Length >= number)
        {
            AudioClip clip = clips[number];

            AddClip(clip);
        }
    }

    void AddClip(AudioClip clip)
    {
        for (int i = 0; i <= sources.Count - 1; i++)
        {
            if (!IsPlaying(sources[i]))
            {
                sources[i].clip = clip;
                sources[i].Play();
                return;
            }
        }

        sources.Add(gameObject.AddComponent<AudioSource>());
    }

    public void PlayRandomBumpEffect()
    {
        if (bumpEffects.Length > 0)
        {
            AudioClip clip = bumpEffects[Random.Range(0, bumpEffects.Length)];

            AddClip(clip);
        }
    }

    bool IsPlaying(AudioSource source)
    {
        if (source.clip == null)
            return false;
        if (!source.isPlaying)
            return false;
        return true;
    }
}