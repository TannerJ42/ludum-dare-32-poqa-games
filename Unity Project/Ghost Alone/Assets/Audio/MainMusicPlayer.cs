﻿using UnityEngine;
using System.Collections;

public class MainMusicPlayer : MonoBehaviour 
{
    public AudioSource source;

	// Use this for initialization
	void Start () 
    {
        DontDestroyOnLoad(transform.gameObject);

        if (source)
        {
            if (MusicShouldBePlaying())
            {
                if (!source.isPlaying)
                    source.Play();
            }
            else
            {
                if (source.isPlaying)
                {
                    source.Stop();
                }
            }
        }
	}

    void OnLevelWasLoaded (int level)
    {
        if (source)
        {
            if (MusicShouldBePlaying())
            {
                if (!source.isPlaying)
                    source.Play();
            }
            else
            {
                if (source.isPlaying)
                {
                    source.Stop();
                }
            }
        }
    }

    private bool MusicShouldBePlaying()
    {
        bool ret = false;

        if (Application.loadedLevel <= 1)
        {
            ret = true;
        }

        return ret;
    }
	
	// Update is called once per frame
	void Update () 
    {
	    
	}
}
